#ifndef RAYTRACING_SPHERE_H
#define RAYTRACING_SPHERE_H

#include <cmath>


#include "hitable.h"


class Sphere : public Hitable
{

private:
    Vec3 center;
    float radius;
    Material *mat_ptr;

public:
    Sphere() = default;
    Sphere(Vec3 cen, float r, Material *m) : center{cen}, radius{r}, mat_ptr{m} {}
    virtual bool hit(const Ray &r, float tmin, float tmax, hit_record &rec) const;

};


bool Sphere::hit(const Ray &r, float tmin, float tmax, hit_record &rec) const
{
    auto oc = r.origin() - center;
    auto a = dot(r.direction(), r.direction());
    auto b = dot(oc, r.direction());
    auto c = dot(oc, oc) - radius * radius;
    auto discriminant = b * b - a * c;

    if (discriminant > 0)
    {
        auto temp = (-b - std::sqrtf(b*b - a*c)) / a;

        if (temp < tmax && temp > tmin)
        {
            rec.t = temp;
            rec.p = r.point_at_parameter(rec.t);
            rec.normal = (rec.p - center) / radius;
            rec.mat_ptr = mat_ptr;

            return true;
        }

        temp = (-b + std::sqrtf(b*b - a*c)) / a;

        if (temp < tmax && temp > tmin)
        {
            rec.t = temp;
            rec.p = r.point_at_parameter(rec.t);
            rec.normal = (rec.p - center) / radius;
            rec.mat_ptr = mat_ptr;

            return true;
        }
    }

    return false;
}


#endif //RAYTRACING_SPHERE_H
