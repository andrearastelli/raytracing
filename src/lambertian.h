#ifndef RAYTRACING_LAMBERTIAN_H
#define RAYTRACING_LAMBERTIAN_H


#include "material.h"


class Lambertian : public Material
{

private:
    Color albedo;

public:
    Lambertian() = delete;
    Lambertian(const Color &a) : albedo{a} {}

    virtual bool scatter(const Ray &r_in, const hit_record &rec, Color &attenuation, Ray &scattered) const;

};


bool Lambertian::scatter(const Ray &r_in, const hit_record &rec, Color &attenuation, Ray &scattered) const
{
    auto target = rec.p + rec.normal + random_in_unit_sphere();
    scattered = Ray{rec.p, target - rec.p};
    attenuation = albedo;

    return true;
}


#endif //RAYTRACING_LAMBERTIAN_H
