#ifndef RAYTRACING_HITABLELIST_H
#define RAYTRACING_HITABLELIST_H


#include "hitable.h"


class HitableList : public Hitable
{

private:
    Hitable **list;
    int list_size;

public:
    HitableList() = default;
    HitableList(Hitable **l, int n) : list{l}, list_size{n} {}
    virtual bool hit(const Ray &r, float tmin, float tmax, hit_record &rec) const;

};


bool HitableList::hit(const Ray &r, float tmin, float tmax, hit_record &rec) const
{
    hit_record temp_rec;

    auto hit_anything = false;
    auto closest_so_far = tmax;

    for (int i=0; i<list_size; ++i)
    {
        if (list[i]->hit(r, tmin, closest_so_far, temp_rec))
        {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }

    return hit_anything;
}


#endif //RAYTRACING_HITABLELIST_H
