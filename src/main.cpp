#include <iostream>
#include <array>
#include <thread>
#include <random>
#include <chrono>
#include <list>


#include "image.h"
#include "vec3.h"
#include "ray.h"
#include "hitablelist.h"
#include "sphere.h"
#include "camera.h"
#include "lambertian.h"
#include "metal.h"
#include "dielectric.h"


Color ray_color(const Ray &r)
{
    auto t = hit_sphere(Vec3(0.0f, 0.0f, -1.0f), 0.5, r);
    if (t > 0.0f)
    {
        auto N = unit_vector(r.point_at_parameter(t) - Vec3(0.0f, 0.0f, -1.0f));
        return 0.5f * Vec3(N.x()+1, N.y()+1, N.z()+1);
    }

    auto unit_direction = unit_vector(r.direction());
    t = 0.5f * (unit_direction.y() + 1.0f);
    return (1.0f - t) * Vec3(1.0f, 1.0f, 1.0f) + t * Vec3(0.5f, 0.7f, 1.0f);
}


Color ray_color2(const Ray &r, Hitable *world, int depth)
{
    hit_record rec;

    if (world->hit(r, 0.001f, MAXFLOAT, rec))
    {
        Ray scattered;
        Color attenuation;

        if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
        {
            return Color(attenuation) * ray_color2(scattered, world, depth+1);
        }
        else {
            return Color{0.0f, 0.0f, 0.0f};
        }
    }
    else
    {
        auto unit_direction = unit_vector(r.direction());
        auto t = 0.5f * (unit_direction.y() + 1.0f);
        return (1.0f - t) * Vec3(1.0f, 1.0f, 1.0f) + t * Vec3(0.5f, 0.7f, 1.0f);
    }
}

#define SIZE 80000
void inner_loop(int idY, int width, int height, int size, int samples, Camera cam, Hitable *world, std::array<Color, SIZE> &colors)
{
    for (int idX = 0; idX < width; ++idX)
    {

        Color c{0, 0, 0};

        for (int s = 0; s < samples; ++s)
        {
            auto u = (idX + dist(m)) / static_cast<float>(width);
            auto v = (idY + dist(m)) / static_cast<float>(height);

            auto r = cam.get_ray(u, v);
            auto p = r.point_at_parameter(2.0f);

            c += ray_color2(r, world, 0);
        }

        c /= static_cast<float>(samples);


        auto idx = size - (width * idY + idX);
        colors[idx] = c;

    }
}


Hitable* random_scene()
{
    auto n = 500;
    auto **list = new Hitable*[n+1];
    list[0] = new Sphere(Vec3(0.0f, -1000.0f, 0.0f), 1000, new Lambertian(Vec3(0.5f, 0.5f, 0.5f)));
    auto i = 1;

    for (int a=0; a<21; ++a)
    {
        for (int b=0; b<21; ++b)
        {
            auto pick_mat = drand48();
            auto radius = static_cast<float>(0.2f + drand48() * 0.5f);

            Material *m;

            if (pick_mat < 0.7f)
                m = new Metal(
                        Vec3(0.5f * (1 + drand48()), 0.5f * (1 + drand48()), 0.5f * (1 + drand48())),
                        0.5f * drand48()
                );
            else //if (pick_mat < 0.9f)
                m = new Lambertian(
                        Vec3(0.5f * (1 + drand48()), 0.5f * (1 + drand48()), 0.5f * (1 + drand48()))
                );

            list[i++] = new Sphere(
                Vec3(a-10.5f, 0.2f + drand48()*1.0f, b-10.5f),
                radius,
                m
            );
        }
    }

    return new HitableList(list, i);
}


int main()
{
    std::cout << "Num threads: " << std::thread::hardware_concurrency() << std::endl;

    Image i("test_dielectric_3.ppm", 400, 10);

    auto samples = 4;

    Hitable *world = random_scene();

    Camera cam(
            Vec3(-3.0f, 1.5f, 2.0f),
            Vec3(0.0f, 0.0f, -1.0f),
            Vec3(0.0f, 1.0f, 0.0f),
            45,
            static_cast<float>(i.width()) / static_cast<float>(i.height()),
            0.05f,
            (Vec3(3.0f, 3.0f, 2.0f) - Vec3(0.0f, 0.0f, -1.0f)).length()
    );

    static constexpr int size = 400 * 200;
    std::array<Color, size> colors;

    auto now = std::chrono::system_clock::now();

    std::list<std::thread> thread_pool;

    for (int idY=i.height() - 1; idY>=0; idY-=std::thread::hardware_concurrency())
    {
        thread_pool.emplace_back(std::thread(inner_loop, idY-0, i.width(), i.height(), size, samples, cam, world, std::ref(colors)));
        thread_pool.emplace_back(std::thread(inner_loop, idY-1, i.width(), i.height(), size, samples, cam, world, std::ref(colors)));
        thread_pool.emplace_back(std::thread(inner_loop, idY-2, i.width(), i.height(), size, samples, cam, world, std::ref(colors)));
        thread_pool.emplace_back(std::thread(inner_loop, idY-3, i.width(), i.height(), size, samples, cam, world, std::ref(colors)));

        for (auto &t : thread_pool)
        {
            t.detach();
        }

        thread_pool.clear();
    }

    auto end = std::chrono::system_clock::now();

    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - now);
    auto s = std::chrono::duration_cast<std::chrono::seconds>(end-now);

    std::cout << "Duration: " << ms.count() << "ms" << std::endl;
    std::cout << "Duration in sec: " << s.count() << "s" << std::endl;


    for (auto &c: colors)
    {
        i.write(c);
    }


    return 0;
}
