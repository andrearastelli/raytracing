#ifndef RAYTRACING_METAL_H
#define RAYTRACING_METAL_H


#include "material.h"


class Metal : public Material
{

private:
    Color albedo;
    float fuzz;

public:
    Metal() = delete;
    Metal(const Color &a, float f) : albedo{a}, fuzz{f} { if (f>=1) fuzz = 1; }
    virtual bool scatter(const Ray &r_in, const hit_record &rec, Color &attenuation, Ray &scattered) const;

};


bool Metal::scatter(const Ray &r_in, const hit_record &rec, Color &attenuation, Ray &scattered) const
{
    auto reflected = reflect(unit_vector(r_in.direction()), rec.normal);
    scattered = Ray{rec.p, reflected + fuzz*random_in_unit_sphere()};
    attenuation = albedo;

    return (dot(scattered.direction(), rec.normal) > 0);
}


#endif //RAYTRACING_METAL_H
