#ifndef RAYTRACING_CAMERA_H
#define RAYTRACING_CAMERA_H


#include "ray.h"


Vec3 random_in_unit_disc()
{
    Vec3 p;
    do {
        p = 2.0f * Vec3(drand48(), drand48(), 0.0f) - Vec3(1.0f, 1.0f, 0.0f);
    } while (dot(p, p) >= 1.0f);

    return p;
}


class Camera
{

private:
    Vec3 origin;
    Vec3 lower_left_corner;
    Vec3 horizontal;
    Vec3 vertical;
    Vec3 u, v, w;
    float lens_radius;

public:
    Camera() : lower_left_corner{-2.0f, -1.0f, -1.0f}, horizontal{4.0f, 0.0f, 0.0f}, vertical{0.0f, 2.0f, 0.0f}, origin{0.0f, 0.0f, 0.0f} {}
    Camera(Vec3 lookfrom, Vec3 lookat, Vec3 up, float vfov, float aspect, float aperture, float defocus_dist);
    Ray get_ray(float u, float v);

};


Camera::Camera(Vec3 lookfrom, Vec3 lookat, Vec3 up, float vfov, float aspect, float aperture, float defocus_dist)
{
    lens_radius = aperture / 2.0f;
    auto theta = static_cast<float>(vfov * M_PI / 180.0f);
    auto half_height = std::tanf(theta / 2.0f);
    auto half_width = aspect * half_height;

    origin = lookfrom;
    w = unit_vector(lookfrom - lookat);
    u = unit_vector(cross(up, w));
    v = cross(w, u);

    lower_left_corner = origin - half_width * defocus_dist * u - half_height * defocus_dist * v - defocus_dist * w;
    horizontal = 2 * half_width * defocus_dist * u;
    vertical = 2 * half_height * defocus_dist * v;
}


Ray Camera::get_ray(float s, float t)
{
    Vec3 rd = lens_radius * random_in_unit_disc();
    Vec3 offset = rd.x() * u + rd.y() * v;
    return {
        origin + offset,
        lower_left_corner + (s * horizontal) + (t * vertical) - origin - offset
    };
}


#endif //RAYTRACING_CAMERA_H
