#ifndef RAYTRACING_MATERIAL_H
#define RAYTRACING_MATERIAL_H


#include "hitable.h"
#include "ray.h"
#include "vec3.h"


std::random_device d;
std::mt19937 m{d()};
std::uniform_real_distribution<float> dist(0.0, 1.0);


class Material
{

public:
    virtual bool scatter(const Ray& r_in, const hit_record &rec, Color &attenuation, Ray &scattered) const = 0;

};


Vec3 random_in_unit_sphere()
{
    Vec3 p;

    do {
        auto x = dist(m);
        auto y = dist(m);
        auto z = dist(m);
        p = 2.0f * Vec3(x, y, z) - Vec3(1.0f, 1.0f, 1.0f);
    } while (p.squared_length() >= 1.0f);

    return p;
}


Vec3 reflect(const Vec3 &v, const Vec3 &n)
{
    return v - 2 * dot(v, n) * n;
}


bool refract(const Vec3 &v, const Vec3 &n, float ni_over_nt, Vec3 &refracted)
{
    Vec3 uv = unit_vector(v);
    auto dt = dot(uv, n);
    auto discriminant = 1.0f - ni_over_nt * ni_over_nt * (1 - dt * dt);

    if (discriminant > 0)
    {
        refracted = ni_over_nt * (uv - n * dt) - n * std::sqrtf(discriminant);
        return true;
    }

    return false;
}


float schlick(float cosine, float ref_idx)
{
    auto r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 = r0 * r0;

    return r0 + (1 - r0) * std::powf((1 - cosine), 5);
}


#endif //RAYTRACING_MATERIAL_H
